#!/bin/env make -f
# CREDIT: Much of this build system was inspired by and/or directly stolen from
# the Linux kernel.

VERSION_MAJOR = 0
VERSION_MINOR = 2
VERSION_PATCH = 1
VERSION_EXTRA =

DOTFILE_VERSION = ${VERSION_MAJOR}$(if ${VERSION_MINOR},.${VERSION_MINOR}$(if ${VERSION_PATCH},.${VERSION_PATCH}$(if ${VERSION_EXTRA},.${VERSION_EXTRA})))
export VERSION_MAJOR VERSION_MINOR VERSION_PATCH VERSION_EXTRA DOTFILE_VERSION

PHONY :=

PHONY += all
all:
.DELETE_ON_ERROR:

$(if $(filter __%, ${MAKECMDGOALS}), \
	$(error targets prefixed with '__' are only for internal use))

# Disable built-in rules (-r) & variables (-R)
MAKEFLAGS += -rR

# Setup $outdir
#
# Examples:
#     # Method 1:
#     cd out; make -f ../Makefile
#
#     # Method 2:
#     make O=out/
#
#     # Method 3:
#     export KBUILD_OUTPUT=out/; make
ifeq ("$(origin O)", "command line")
KBUILD_OUTPUT := $(realpath ${O})
endif

ifneq (${KBUILD_OUTPUT},)
abs_outdir := $(shell mkdir -p ${KBUILD_OUTPUT} && cd ${KBUILD_OUTPUT} && pwd)
$(if ${abs_outdir},, \
	$(error failed to create output directory))

abs_outdir := $(realpath ${abs_outdir})
else
abs_outdir := ${CURDIR}
KBUILD_OUTPUT := ${abs_outdir}
endif

ifneq (${KBUILD_DOWNLOADS},)
abs_dldir := $(shell mkdir -p ${KBUILD_DOWNLOADS} && cd ${KBUILD_DOWNLOADS} && pwd)
$(if ${abs_dldir},, \
	$(error failed to create downloads directory))

abs_dldir := $(realpath ${abs_dldir})
else
abs_dldir := ${abs_outdir}/.dl
KBUILD_DOWNLOADS := ${abs_dldir}
endif

# Setup $abs_srctree
me := $(lastword ${MAKEFILE_LIST})
abs_srctree := $(realpath $(dir ${me}))
export abs_srctree abs_outdir KBUILD_OUTPUT abs_dldir KBUILD_DOWNLOADS

ifeq ("$(origin S)", "command line")
KBUILD_TARGET := ${S}
endif
ifeq (${KBUILD_TARGET},)
KBUILD_TARGET := ${abs_srctree}
endif

KBUILD_TARGET := $(realpath ${KBUILD_TARGET})
abs_srcdir := ${KBUILD_TARGET}
VPATH := ${abs_srcdir}
export KBUILD_TARGET abs_srcdir VPATH

ifneq (${abs_srctree},${abs_srcdir})
MAKEFLAGS += --include-dir=${abs_srctree}
endif
ifeq ("${abs_outdir}", "${CURDIR}")
MAKEFLAGS += --no-print-directory
endif
export MAKEFLAGS

# Setup DESTDIR for staged installs
ifeq ("$(origin D)", "command line")
DESTDIR = ${D}
endif
export DESTDIR

# Logging
ifeq ("$(origin V)", "command line")
KBUILD_VERBOSE = ${V}
endif
ifndef KBUILD_VERBOSE
KBUILD_VERBOSE = 0
endif

ifeq (${KBUILD_VERBOSE},0)
Q = @
quiet = quiet_
else
Q =
quiet =
endif

ifeq ($(findstring s, $(firstword -${MAKEFLAGS})),s)
KBUILD_VERBOSE = 0
quiet = silent_
endif
export KBUILD_VERBOSE Q quiet

ifeq ($(findstring n, $(firstword -${MAKEFLAGS})),n)
KBUILD_DEBUG = 1
endif
ifndef KBUILD_DEBUG
KBUILD_DEBUG = 0
endif
export KBUILD_DEBUG

include ${abs_srctree}/scripts/Makefile.lib

# Platform-specific stuff
include ${abs_srctree}/scripts/platform.include

PLATFORM ?= $(SRCPLATFORM)
PLATFORM_KBUILD_INCLUDE = $(wildcard ${abs_srctree}/platform/${PLATFORM}/Kbuild.include)
export PLATFORM PLATFORM_KBUILD_INCLUDE

# Configuration
ifdef KCONFIG_CONFIG
KCONFIG_CONFIG := $(realpath ${KCONFIG_CONFIG})
export KCONFIG_CONFIG
endif

# Build targets
build-target := $(call relative-to-curdir, ${KBUILD_TARGET})

CROSS_COMPILE := $(or ${CROSS_COMPILE}, ${XCOMPILE})
export CROSS_COMPILE

KBUILD_CFLAGS = ${CFLAGS}
KBUILD_EXTRACFLAGS = ${EXTRACFLAGS}
KBUILD_HOST_CFLAGS = $(if ${CROSS_COMPILE}${XCOMPILE},,${KBUILD_CFLAGS}) ${HOST_CFLAGS}
KBUILD_HOST_EXTRACFLAGS = $(if ${CROSS_COMPILE}${XCOMPILE},,${KBUILD_EXTRACFLAGS}) ${HOST_EXTRACFLAGS}
export KBUILD_CFLAGS KBUILD_EXTRACFLAGS KBUILD_HOST_CFLAGS KBUILD_HOST_EXTRACFLAGS

KBUILD_LDFLAGS = ${LDFLAGS}
KBUILD_EXTRALDFLAGS = ${EXTRALDFLAGS}
KBUILD_HOST_LDFLAGS = $(if ${CROSS_COMPILE}${XCOMPILE},,${KBUILD_LDFLAGS}) ${HOST_LDFLAGS}
KBUILD_HOST_EXTRALDFLAGS = $(if ${CROSS_COMPILE}${XCOMPILE},,${KBUILD_EXTRALDFLAGS}) ${HOST_EXTRALDFLAGS}
export KBUILD_LDFLAGS KBUILD_EXTRALDFLAGS KBUILD_HOST_LDFLAGS KBUILD_HOST_EXTRALDFLAGS

KBUILD_CPPFLAGS = ${CPPFLAGS}
KBUILD_EXTRACPPFLAGS = ${EXTRACPPFLAGS}
export KBUILD_CPPFLAGS KBUILD_EXTRACPPFLAGS

PHONY += all
all: ${build-target}
	$(noop)

PHONY += version
version:
	@echo "${DOTFILE_VERSION}"

PHONY += clean
clean:
	${Q}$(MAKE) -C ${abs_outdir} ${do_clean}=src

PHONY += distclean
distclean:
	${Q}$(MAKE) -C ${abs_outdir} ${do_clean}=.
	${Q}rm -rf ${abs_dldir} ${abs_outdir}/MANIFEST.$(strip $(CHECKSUM))

PHONY += ${build-target}
${build-target}:
	${Q}$(MAKE) -C ${abs_outdir} ${do_build}=$@ ${MAKECMDGOALS}

# Distribution targets

      cmd_tar = tar
quiet_cmd_tar = TAR         $@
tar = $(call cmd,tar)

# HOST_IS_TARGET by default unless using "dist" as the target
export HOST_IS_TARGET := y
PHONY += dist
dist: export HOST_IS_TARGET := n
dist: ${abs_outdir}/dotfiles-${DOTFILE_VERSION}.tar.gz
	$(noop)

${abs_outdir}/dotfiles-${DOTFILE_VERSION}.tar.gz: ${abs_outdir}/dotfiles-${DOTFILE_VERSION}
	$(tar) -czf $@ -C $^ '*'

PHONY += ${abs_outdir}/dotfiles-${DOTFILE_VERSION}
${abs_outdir}/dotfiles-${DOTFILE_VERSION}:
	${Q}mkdir -p $@
	${Q}$(MAKE) -C ${abs_outdir} ${do_install}=src DESTDIR=$@

# Config targets

PHONY += config %config

config: FORCE
	${Q}$(MAKE) ${do_build}=utils/kconfig $@

%config: FORCE
	${Q}$(MAKE) ${do_build}=utils/kconfig $@

# Test targets

PHONY += test test-%

test: FORCE
	${Q}$(MAKE) -f ${abs_srctree}/scripts/Makefile.test target= $@

test-%: FORCE
	${Q}$(MAKE) -f ${abs_srctree}/scripts/Makefile.test target= $@

# Info targets

PHONY += help
help:
	@echo 'Build targets:'
	@echo '  all'
	@echo '  <OUTDIR>/<PATH>'
	@echo ''
	@echo 'Cleaning targets:'
	@echo '  clean'
	@echo '  distclean'
	@echo ''
	@echo 'Configuration'
	@echo '  menuconfig'
	@echo '  config'
	@echo '  defconfig'
	@echo '  savedefconfig'
	@echo '  oldconfig'
	@echo '  allnoconfig'
	@echo '  allyesconfig'
	@echo '  alldefconfig'
	@echo '  randconfig'
	@echo '  listnewconfig'
	@echo ''
	@echo 'Distribution targets:'
	@echo '  [re]install [<OUTDIR>/<PATH>]'
	@echo '  uninstall'
	@echo '  dist'
	@echo '  version'
ifneq ($(call host-prog-find, bear),)
	@echo ''
	@echo 'Development hints:'
	@echo '  bear -- make -B [TARGET]'
endif
	@echo ''
	@echo 'Tweaks:'
	@echo '  O=<OUTDIR>'
	@echo '  S=<TARGET>'
	@echo '  D=<DESTDIR>'
	@echo '  V=0|1'

install-is-noop :=
PHONY += install uninstall reinstall
real-goals := $(filter-out ${PHONY},${MAKECMDGOALS})
ifneq (${real-goals},)

ifeq ($(filter install uninstall reinstall,$(firstword ${MAKECMDGOALS})),)
do_real-goal = ${do_build}
else
install-is-noop = y
do_real-goal = ${do_install}

KBUILD_REINSTALL=$(if $(filter reinstall, $(firstword ${MAKECMDGOALS})), 1)
export KBUILD_REINSTALL
endif

PHONY += ${real-goals}
${real-goals}: __real_goals_one_by_one
	$(noop)

PHONY += __real_goals_one_by_one
__real_goals_one_by_one:
	${Q}set -e; \
	for i in $(foreach g, ${real-goals}, $(call relative-to-dir,${g},${abs_outdir})); do \
		$(MAKE) -C ${abs_outdir} ${do_real-goal}=. $$i; \
	done
endif

# Install targets
ifeq (${install-is-noop},y)
install:; $(noop)
reinstall:; $(noop)
else
install:
	${Q}$(MAKE) -C ${abs_outdir} ${do_install}=.

reinstall:
	${Q}$(MAKE) -C ${abs_outdir} ${do_install}=. KBUILD_REINSTALL=1
endif

uninstall:
	${Q}$(MAKE) -C ${abs_outdir} ${do_uninstall}=.

.PHONY: ${PHONY}
