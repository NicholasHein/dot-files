# Dot Files

This is my personal dot files repo.
It contains configurations default configurations for:

- NeoVim
- Zsh
- Git
- CTags
- Terminator

To learn more about my other work, check out [my website](//paradoxdev.com)!

## Usage

To use this project, clone it to any directory with:

```bash
  $ git clone https://gitlab.com/NicholasHein/dot-files.git
```

Then, to install it, run the install script to copy the files to the default staging location:

```bash
  $ ./install.sh
```

*Note: you can change this location to anything you want. See `./install.sh --help`*

**Warning: this next step may break things!**

To link your system's configurations to the staging directory:

```bash
  $ ./install.sh -l
```

## To-do

  1. Use [m4](https://www.gnu.org/software/m4/manual/m4.html) for adapting source files
  2. Use [sed(1)](man://sed(1)) for integrating system configurations
  3. Separate into independent packages
  4. Replace CRON jobs with [systemd.timer(5)](man://systemd.timer(5))

## Dependencies

*Note:* This section is a work in progress and will eventually switch to automatic installation from submodules

### Neovim

```bash
  $ sudo pacman -S neovim neovim-qt python-pynvim
```

### Language Servers

__jsonls__

```bash
  $ sudo npm i -g vscode-langservers-extracted
```

## Notes

There are some files that are purposefully untracked due to privacy issues but
may be required by some programs.  But it is also nice to store these in the
project's tree so they are never overwritten and are instead automatically
installed with the rest of the project's files.  So, it is recommended to
override the individual `.gitignore` files so these untracked files aren't
removed by an unintentional `git clean`.  To do this, add these lines to the
bottom of your Git exclude file:

`.git/info/exclude`:

```gitignore
  ...
  ############################################################
    ####################### INCLUDES #######################
  ############################################################

  !/alacritty/overrides.yml

  !/bash/bash.d/user/**

  !/cron/custom/**
  !/cron/enable.conf

  !/git/git.d/config/custom/**
  !/git/git_template/hooks/custom/**

  !/neomutt/contacts/**
  !/neomutt/passwords/**
  !/neomutt/private/**

  !/nvim/custom.vim

  !/zsh/aliases/custom.zsh
  !/zsh/plugins/custom.zsh
  !/zsh/env/user.zsh
```

## Contributing

All commits to this project as of August 6th, 2022 must follow the
["Conventional Commit" format](https://www.conventionalcommits.org).

## Credit

- [Christian Chiarulli's own dot-files project](https://github.com/ChristianChiarulli/Machfiles) inspired the scope and layout for this project
- [tpope's "Effortless Ctags with Git"](https://tbaggery.com/2011/08/08/effortless-ctags-with-git.html) inspired some git hooks/aliases
- [samoshkin's tmux configuration](https://github.com/samoshkin/tmux-config)
