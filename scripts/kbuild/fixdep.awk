# Usage: awk -v name=asdf.o -v target=utils/foo/asdf.o -f scripts/kbuild/fixdep.awk

BEGIN {
	FS = " "
	print ""
}

NR == 1 {
	line = gensub("^" name ": (.*)", "deps_" target " := \\1", 1, $0)
	print line
}
NR != 1 { print }

END {
	print ""
	print target ":", "$(deps_" target ")"
	print ""
	print "$(deps_" target "):"
}
