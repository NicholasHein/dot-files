#!/usr/bin/env python3

import sys
import urllib.parse

input = [f"{w}\n" for w in sys.argv[1::]] or sys.stdin
sys.stdout.writelines(urllib.parse.quote(word, safe="\n") for word in input)
