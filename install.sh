#!/usr/bin/env zsh

PROJECT_NAME="dot-files"
STAGING_DIR="${PROJECT_NAME}"
TARGET_DIR="${PROJECT_NAME}.target.d"

function print_usage()
{
	echo -e \
		"Usage $0 [<option>] [<DIRECTORY>]\n" \
		"Install the configuration to the system.\n" \
		"\n" \
		"If no options are included, '--stage' is implied.\n" \
		"DIRECTORY refers to the system staging directory.\n" \
		"The default DIRECTORY is '\$HOME/.config/${PROJECT_NAME}'.\n" \
		"Note: If DIRECTORY is included, the files are copied to that directory rather than the default.\n" \
		"\n" \
		"OPTIONS:\n" \
		"\t-s, --stage\t Stage the files to the system location\n" \
		"\t-l, --link\t Link the staging directory to the system's configs\n" \
		"\t-c, --check\t Check if the system is configured already\n" \
		"\t-x, --sync\t Synchronize packages and plugins\n" \
		"\t-t <target> [ARGS], --test <target> [ARGS]\n" \
		"\t\t\tTest the settings here without making modifications to the system\n" \
		"\t\t\t  <target>:\t the program to test with this configuration\n" \
		"\t\t\t  ARGS:\t\t if included, everything here is passed to the command under test\n" \
		"\t-h, --help\t Print this message" \
		"\n" \
		"PROGRAMS:\n" \
		"\t - tmux\n" \
		"" 1>&2
}

################################################################################
# OPERATIONS
################################################################################
function cp_res()
{
	mkdir -p "$DOTFILES_DIR"

	# Copy resources
	cp -r -v "$PROJECT_DIR/src/tmux" "$DOTFILES_DIR"

	make -C "$PROJECT_DIR"/src/scripts install

	echo "`git rev-parse --verify HEAD`" > "$DOTFILES_DIR/.rev"
}

function link_target()
{
	ln -snri "$DOTFILES_DIR" "$DOTFILES_TARGET"
}

function config_fs()
{
	# DOCS:
	# TODO: create man pages or something?

	# TMUX:
	ln -snri "$DOTFILES_TARGET/tmux"		"$HOME/.config/"
	ln -snri "$DOTFILES_TARGET/tmux/tmux.conf"	"$HOME/.tmux.conf"
}

function init_fs()
{
	# TMUX:
	mkdir -p \
		"$DOTFILES_DIR/tmux/plugins" \
		"$DOTFILES_DATA_DIR/tmux" \
		"$DOTFILES_DATA_DIR/tmux/sessions"
}

function init_project ()
{
	# TMUX:
	TMUX_DIR="$HOME/.config/tmux"
	TMUX_PLUG_DIR="$TMUX_DIR/plugins"
	set -x
	eval "tmux new -d -s __noop >/dev/null 2>&1"
	eval "tmux set-environment -g TMUX_PLUGIN_MANAGER_PATH '$TMUX_PLUG_DIR'"
	eval "$TMUX_PLUG_DIR/tpm/bin/install_plugins"
	eval "tmux kill-session -t __noop >/dev/null 2>&1"
	set +x
}

################################################################################
# SCRIPT SUB-COMMANDS
################################################################################

function run_install()
{
	if [ $# -eq 1 ]; then
		DOTFILES_DIR="$1"
	elif [ $# -gt 1 ]; then
		print_usage
		exit 1
	fi

	echo -e "Run: installing..."
	echo -e "Staging directory:\t '$DOTFILES_DIR'"
	echo ""

	echo "Copying resources..."
	cp_res
}

function run_stage()
{
	if [ $# -eq 1 ]; then
		DOTFILES_DIR="$1"
	elif [ $# -gt 1 ]; then
		print_usage
		exit 1
	fi

	echo -e "Run: staging..."
	echo -e "Staging directory:\t '$DOTFILES_DIR'"
	echo ""

	echo "Copying resources..."
	cp_res
	echo "Initializing target..."
	init_fs
	echo "Updaing programs..."
	init_project
}

function run_link()
{
	if [ $# -eq 1 ]; then
		DOTFILES_DIR="$1"
	elif [ $# -gt 1 ]; then
		print_usage
		exit 1
	fi

	echo -e "Run: linking..."
	echo -e "Staging directory:\t '$DOTFILES_DIR'"
	echo -e "Target link:\t\t '$DOTFILES_TARGET'"
	echo ""
	echo -e "WARNING: This modifies important user files!"
	echo -e "WARNING: Be sure this will not destroy your current user configurations."

	read -s -q "CONF?Continue (Y/n)?"
	if ! [[ "$CONF" =~ ^[Yy]$ ]]; then
		echo "Aborting"
		exit 0
	fi

	echo ""
	echo "Creating the symlink..."
	link_target
	echo "Configuring the filesystem..."
	config_fs
}

function run_test()
{
	TEST_TMUX_PATH="$PROJECT_DIR/src/tmux/tmux.conf"

	if [ $# -lt 1 ]; then
		print_usage
		exit 1
	fi

	echo -e "Run: testing..."

	TEST_TARGET="${1:l}"
	shift

	case "$TEST_TARGET" in
		'tmux')
			echo "Testing target: Tmux..."
			echo ""
			echo "Tmux: running with config '$TEST_TMUX_PATH'..."

			set -x
			eval "tmux -f '$TEST_TMUX_PATH' $@"
			set +x

			echo "Tmux: exited."
			;;
		*)
			echo -e "Unknown testing target '$TEST_TARGET'!" 1>&2
			exit 2
			;;
	esac
}

function run_check()
{
	if [ $# -eq 1 ]; then
		DOTFILES_DIR="$1"
	elif [ $# -gt 1 ]; then
		print_usage
		exit 1
	fi

	echo -e "Run: checking..."
	echo -e "Staging directory:\t '$DOTFILES_DIR'"
	echo -e "Target link:\t\t '$DOTFILES_TARGET'"
	echo ""

	set +e

	TARGET_LINK="`readlink -e $DOTFILES_TARGET`"
	if ! [ $? -eq 0 ] || ! [[ "$TARGET_LINK" == "$DOTFILES_DIR" ]]
	then
		echo "ERROR: dot-files target link does not exist!" >&2
		exit 3
	fi

	TARGET_REV="`cat $DOTFILES_DIR/.rev`"
	if ! [ $? -eq 0 ] || ! [[ "$TARGET_REV" == "`git rev-parse HEAD`" ]]
	then
		echo "ERROR: target has an out-of-date revision hash!" >&2
		exit 4
	fi

	declare -a CONFIG_LINKS=( \
		"$HOME/.config/tmux" \
		"$HOME/.tmux.conf" \
	)

	FAILURE=0
	for link in "${CONFIG_LINKS[@]}"
	do
		link_target="`readlink -e $link`"
		if ! [ $? -eq 0 ] || ! [[ "$link_target" == *"$TARGET_LINK"* ]]
		then
			FAILURE=1
			echo "WARNING: '$link' not configured properly!" >&2
		fi
	done

	if [ $FAILURE -eq 0 ]
	then
		echo "All checks passed." >&2
	else
		echo "ERROR: Some checks failed!" >&2
		exit 5
	fi

	set -e
}

function run_sync()
{
	if [ $# -eq 1 ]; then
		DOTFILES_DIR="$1"
	elif [ $# -gt 1 ]; then
		print_usage
		exit 1
	fi

	echo -e "Run: syncing..."
	echo -e "Staging directory:\t '$DOTFILES_DIR'"
	echo -e "Target link:\t\t '$DOTFILES_TARGET'"
	echo ""

	echo -e "Syncing plugins..."
	init_project
}

################################################################################
# SCRIPT CONTROL
################################################################################
PROJECT_DIR="`cd "$(dirname ${(%):-%N})" &> /dev/null && pwd`"
DOTFILES_CONFIG_DIR="${XDG_CONFIG_HOME:-$HOME/.config}"
DOTFILES_DATA_DIR="${XDG_DATA_HOME:-$HOME/.local/share}"
DOTFILES_DIR="${DOTFILES_CONFIG_DIR}/${PROJECT_NAME}"
DOTFILES_TARGET="${DOTFILES_CONFIG_DIR}/${TARGET_DIR}"

set -e

case "$1" in
	'-t' | '--test')
		shift
		run_test $@
		;;
	'-s' | '--stage')
		shift
		run_stage $@
		;;
	'-l' | '--link')
		shift
		run_link $@
		;;
	'-c' | '--check')
		shift
		run_check $@
		;;
	'-x' | '--sync')
		shift
		run_sync
		;;
	'-h' | '--help')
		print_usage
		exit 0
		;;
	*)
		run_install $@
		;;
esac

set +e

echo "Finished!"
exit 0
