/*
 * Heavily inspired by Roman Zippel's implementation of Linux's kconfig lexer.
*/
%option nostdinit noyywrap never-interactive full ecs
%option 8bit nodefault yylineno
%x PLACEHOLDER
%{
#include <stdio.h>
#include <stdlib.h>
#include <stddef.h>
#include <string.h>

#include "internal.h"
#include "parser.tab.h"

#define YY_DECL static int yylex_flex(void)

/* Disable these to prevent compiler warnings about unused functions */
#define YY_NO_INPUT
#define YY_NO_UNPUT

static unsigned int current_line;

static struct {
	char *str;
	size_t len;
} current_string;

static void new_string(const char *src, int len)
{
	current_string.len = len;
	current_string.str = xmalloc(current_string.len + 1);

	memcpy(current_string.str, src, len);
	current_string.str[len] = '\0';
}

static void append_string(const char *more, int len)
{
	int new_len = current_string.len + len;
	current_string.str = xrealloc(current_string.str, new_len + 1);

	memcpy(current_string.str + current_string.len, more, len);
	current_string.len += len;
	current_string.str[current_string.len] = '\0';
}

static void die(const char *reason, char chr) {
	fprintf(stderr, "error: %s '%c' [line: %d]\n",
		reason, chr, yylineno);
	exit(EXIT_FAILURE);
}
%}

identifier	[0-9a-zA-Z_]+

%%
		struct genconf_placeholder current_placeholder;

"\\$("		return T_ESC_OPEN_DOLLAR;
"\\@"		return T_ESC_OPEN_AT;
"$("|"@"	{
			int ret = genconf_placeholder_open(&current_placeholder, yytext, yyleng);
			if (ret < 0) {
				ECHO;
			} else {
				new_string(0, 0);
				BEGIN(PLACEHOLDER);
			}
		}

<PLACEHOLDER>{
	{identifier} {
		append_string(yytext, yyleng);
	}
	")"|"@" {
		int ret = genconf_placeholder_close(&current_placeholder, yytext, yyleng);
		if (ret < 0) {
			die("placeholder incorrectly terminated", yytext[0]);
		} else {
			BEGIN(INITIAL);
			yylval.string = current_string.str;
			return T_PLACEHOLDER;
		}
	}
	. {
		die("unexpected character", yytext[0]);
	}
}

<*>.|\n		ECHO;
%%

int yylex(void)
{
	int token = yylex_flex();
	current_line = yylineno;
	return token;
}

unsigned int genconf_current_line(void)
{
	return current_line;
}
