%{
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <stdbool.h>

#include "internal.h"

#define debug(fmt...) do { if(genconf_debug_enabled(GENCONF_DEBUG_BASIC)) fprintf(stderr, fmt); } while(0)

extern int yylex(void);

static void yyerror(const char *err)
{
	fprintf(stderr, "error: %s [line: %u]\n", err, genconf_current_line());
}

extern FILE *yyin;
extern FILE *yyout;
extern int yylineno;
%}

%union
{
	char *string;
	const char *const_string;
}

%token <string> T_PLACEHOLDER
%token T_ESC_OPEN_DOLLAR
%token T_ESC_OPEN_AT

%type <const_string> token default
%type <const_string> placeholder
%type <const_string> escaped

%%
input: %empty | input default;

default: token { fprintf(yyout, "%s", $1); }
token: placeholder | escaped ;

placeholder: T_PLACEHOLDER
{
	const char *subst = genconf_placeholder_subst($1);
	debug("[line %u] debug: placeholder '%s' ==> '%s'\n", genconf_current_line(), ($1), subst);
	$$ = subst;
	free($1);
};

escaped:
	  T_ESC_OPEN_DOLLAR { $$ = "$("; }
	| T_ESC_OPEN_AT     { $$ =  "@"; }
;
%%

int genconf_parse(FILE *infile, FILE *outfile)
{
	yyin = infile;
	yyout = outfile;
	yylineno = 1;
	yydebug = genconf_debug_enabled(GENCONF_DEBUG_PARSER);

	yyparse();
	if (yynerrs)
		return -EINVAL;
	return 0;
}
