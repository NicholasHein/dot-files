#ifndef GENCONF_INTERNAL_H
#define GENCONF_INTERNAL_H

#include <stdio.h>
#include <stddef.h>
#include <stdbool.h>
#include <sys/cdefs.h>

#define array_size(a) (sizeof(a) / sizeof((a)[0]))
#define BIT(n)	      (1 << (n))

int yylex(void);
int genconf_parse(FILE *infile, FILE *outfile);

void *xmalloc(size_t size) __attribute_malloc__ __attribute_alloc_size__((1));
void *xrealloc(void *ptr, size_t size) __attribute_alloc_size__((2));

struct genconf_placeholder {
	const char *token_open;
	const char *token_close;
};

int genconf_placeholder_open(struct genconf_placeholder *ph, const char *text,
			     size_t textlen);
int genconf_placeholder_close(struct genconf_placeholder *ph, const char *text,
			      size_t textlen);

enum genconf_debug_type {
	GENCONF_DEBUG_BASIC = BIT(0),
	GENCONF_DEBUG_PARSER = BIT(1),
};

bool genconf_debug_enabled(enum genconf_debug_type type);
unsigned int genconf_current_line(void);

const char *genconf_placeholder_subst(const char *name);

#endif
