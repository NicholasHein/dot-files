#include "internal.h"
#include <getopt.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include <errno.h>
#include <stddef.h>

struct genconf_data {
	int debug;
	struct genconf_placeholder placeholder;
};

static struct genconf_data genconf_data = { 0 };

enum genconf_placeholder_type {
	GENCONF_PLACEHOLDER_DOLLAR,
	GENCONF_PLACEHOLDER_AT,
};

static const struct genconf_placeholder placeholders[] = {
	[GENCONF_PLACEHOLDER_DOLLAR] = { .token_open = "$(",
					 .token_close = ")" },
	[GENCONF_PLACEHOLDER_AT] = { .token_open = "@", .token_close = "@" },
};

void *xmalloc(size_t size)
{
	void *ptr = malloc(size);
	assert(ptr);
	if (!ptr)
		abort();
	return ptr;
}

void *xrealloc(void *ptr, size_t size)
{
	ptr = realloc(ptr, size);
	assert(ptr);
	if (!ptr)
		abort();
	return ptr;
}

int genconf_placeholder_open(struct genconf_placeholder *ph, const char *text,
			     size_t textlen)
{
	assert(ph);
	assert(text);

	if (strncmp(genconf_data.placeholder.token_open, text, textlen) != 0)
		return -EINVAL;

	memcpy(ph, &genconf_data.placeholder, sizeof(*ph));
	return 0;
}
int genconf_placeholder_close(struct genconf_placeholder *ph, const char *text,
			      size_t textlen)
{
	assert(ph);
	assert(ph->token_close);
	assert(text);

	return strncmp(ph->token_close, text, textlen) == 0 ? 0 : -EINVAL;
}

bool genconf_debug_enabled(enum genconf_debug_type type)
{
	return genconf_data.debug & type;
}

const char *genconf_placeholder_subst(const char *name)
{
	return getenv(name) ?: "";
}

static void usage(const char *progname)
{
	fprintf(stderr, "usage: %s [options] [FILE]\n", progname);
	/* clang-format off */
	fprintf(stderr,
"\n"
"Options:\n"
"  -$                  Use placeholders of the form \"$(VAR)\" (default)\n"
"  -@                  Use placeholders of the form \"@VAR@\"\n"
"  -d, --debug         Enable debugging messages\n"
"  -h, --help          Show this help message and exit\n");
	/* clang-format on */
}

int main(int argc, char **argv)
{
	int ret;
	int opt;
	const char *progname = argv[0];
	const char *const short_opts = "$@dh";
	const struct option long_opts[] = {
		{ "help", no_argument, NULL, 'h' },
		{ "debug", no_argument, NULL, 'd' },
		{ NULL, 0, NULL, 0 },
	};

	genconf_data.placeholder = placeholders[0];

	while ((opt = getopt_long(argc, argv, short_opts, long_opts, NULL)) !=
	       -1) {
		switch (opt) {
		case 'h':
			usage(progname);
			exit(0);
			break;
		case 'd':
			genconf_data.debug <<= 1;
			genconf_data.debug |= 1;
			break;
		case '$':
			genconf_data.placeholder =
			    placeholders[GENCONF_PLACEHOLDER_DOLLAR];
			break;
		case '@':
			genconf_data.placeholder =
			    placeholders[GENCONF_PLACEHOLDER_AT];
			break;
		default:
			break;
		}
	}

	argc -= optind;
	argv += optind;
	if (argc && !freopen(argv[0], "r", stdin)) {
		perror("freopen");
		return EXIT_FAILURE;
	}

	ret = genconf_parse(stdin, stdout);
	return ret ? EXIT_FAILURE : EXIT_SUCCESS;
}
