#!/usr/bin/env python3

import sys
import shutil
import tempfile
import pathlib
import textwrap
import argparse
import subprocess
import tomli
import importlib.metadata
from datetime import date
from typing import IO, ContextManager
from contextlib import contextmanager
from vcd import VCDWriter
from vcd.writer import Variable as VCDVariable, VarType as VCDVarType


try:
    VERSION = importlib.metadata.version(__package__ or __name__)
except importlib.metadata.PackageNotFoundError:
    VERSION = "0.0"

__version__ = VERSION


class Config:
    @staticmethod
    def parse_str(arg: str) -> dict[str, any]:
        return tomli.loads(arg)

    def __init__(self, file: IO[str] | None = None):
        self._vars = {}
        self._scopes = {}
        self.meta = {
            "comment": "Serialized by gpio-tracing-logic-analyzer"
        }
        self.output = {
            "timestamps": {
                "relative": True,
                "dup_add_ns": 0,
            },
        }

        cfg = tomli.load(file) if file else {}
        self.apply(**cfg)

    def apply(self, **kwargs: any):
        cfg = dict(**kwargs)

        def update(d: dict, u: dict):
            for k, v in u.items():
                if d.get(k) is not None and not isinstance(v, type(d[k])):
                    raise TypeError()

                if isinstance(v, dict):
                    d[k] = update(d.get(k, {}), v)
                elif v is not None:
                    d[k] = v
            return d

        self.meta = update(self.meta, cfg.get("meta", {}))
        self.output = update(self.output, cfg.get("output", {}))
        self._scopes = update(self._scopes, cfg.get("scopes", {}))

    def _init_vcd_var(self,
                      vcd: VCDWriter,
                      var: dict[str, any],
                      path: list[str] = []):
        line = var.get("line")
        if not line:
            raise ValueError()
        line = int(line)

        name = var.get("name")
        if not name:
            raise ValueError()
        name = str(name)

        if self._vars.get(line):
            print(
                "multiple definitions for line \"{}\"".format(line),
                file=sys.stderr)

        self._vars[line] = vcd.register_var(
            scope=path,
            name=name,
            var_type=var.get("type", VCDVarType.wire),
            size=var.get("size") or 1,
            init=var.get("init"),
        )

    def _init_vcd_scope_vars(self,
                             vcd: VCDWriter,
                             scope_vars: list[dict],
                             path: list[str] = []):
        for var in scope_vars:
            self._init_vcd_var(vcd, var, path)

    def _init_vcd_scope(self,
                        vcd: VCDWriter,
                        scope: dict | list,
                        path: list[str] = []):

        if isinstance(scope, list):
            # Scope vars
            self._init_vcd_scope_vars(vcd, scope, path)
        elif isinstance(scope, dict):
            # Scope containing scopes
            for name, memb in scope:
                self._init_vcd_scope(vcd, memb, [*path, name])
        else:
            print(
                "invalid scope/variable \"{}\"".format(".".join(path)),
                file=sys.stderr)

    def init_vcd(self, outfile: IO[str]) -> VCDWriter:
        vcd = VCDWriter(outfile,
                        timescale="1 ns",
                        date=date.today().strftime("%a %b %e %H:%M:%S %Y"),
                        comment=self.meta["comment"] or None,
                        version=f"gpio-tracing-logic-analyzer v{__version__}",
                        default_scope_type="module",
                        )

        for name, memb in self._scopes.items():
            self._init_vcd_scope(vcd, memb, path=[name])
        return vcd

    def get_vcd_var(self, gpio: int) -> VCDVariable | None:
        return self._vars.get(gpio)


class CommandTracer:
    def __init__(self,
                 command: [str],
                 *,
                 debug: bool = False):
        tracecmd = shutil.which("perf") or shutil.which("trace-cmd")
        if not tracecmd:
            raise RuntimeError(
                "Event tracing command not found: perf or trace-cmd")

        self._tracecmd = tracecmd
        self._command = command
        self._debug = debug
        self._is_recorded = False
        pass

    def record(self, outpath: pathlib.Path):
        subproc = subprocess.Popen(
            args=[self._tracecmd, "record", "-e", "gpio",
                  "-o", str(outpath)],
            stdin=sys.stdin,
            stdout=sys.stderr if self._debug else None,
            stderr=sys.stderr if self._debug else None,
            text=True,
        )
        if subproc.wait() != 0:
            raise subprocess.CalledProcessError()
        else:
            self._is_recorded = True

    @contextmanager
    def report(self, inpath: pathlib.Path) -> ContextManager[IO[str]]:
        if not self._is_recorded:
            raise RuntimeError()

        subproc = subprocess.Popen(
            args=[self._tracecmd, "report", "-i", str(inpath)],
            stdin=sys.stdin,
            stdout=subprocess.PIPE,
            stderr=sys.stderr if self._debug else None,
            text=True,
        )
        with subproc:
            yield subproc.stdout

        ret = subproc.returncode
        assert ret is not None
        if ret != 0:
            raise subprocess.CalledProcessError()

    @contextmanager
    def trace(self) -> ContextManager[IO[str]]:
        with tempfile.NamedTemporaryFile() as tmp:
            self.record(tmp.name)
            yield self.report(tmp)


class LogicAnalyzer:
    def __init__(self,
                 config: Config,
                 *,
                 debug: bool = False,
                 ):
        self._config = config
        self._debug = debug

    def _process_trace_report_line(self, vcd: VCDWriter, line: str):
        if line.startswith(("#", "cpus=")):
            return

        column_names = ["task-pid", "flags", "timestamp", "function", "event"]
        cols = dict(zip(column_names,
                        line.strip().split(
                            maxsplit=len(column_names) - 1)))
        func = cols["function"].rstrip(":")
        event = dict(zip(["gpio", "op", "value"], cols["event"].split()))

        gpio = event["gpio"]
        if func == "gpio_value":
            val = event["value"]
        elif func == "gpio_direction" and event["op"] == "out":
            val = event["value"].strip("()")
        else:
            if self._debug:
                print("skipping \"{}\" event".format(func),
                      file=sys.stderr)
            return

        gpio = int(gpio)
        val = int(val)

        gpio_var = self._config.get_vcd_var(gpio)
        if not gpio_var:
            if self._debug:
                print("GPIO \"{}\" is undefined".format(gpio), file=sys.stderr)
            return

        timestamp = float(cols["timestamp"].rstrip(":"))
        timestamp = int(timestamp * 1000 * 1000 * 1000)  # To nano-secs

        if self._config.output["timestamps"]["relative"]:
            if self._first_timestamp is None:
                self._first_timestamp = timestamp
            timestamp -= self._first_timestamp

        if not self._prev_timestamp:
            self._prev_timestamp = timestamp
        elif self._prev_timestamp >= timestamp:
            timestamp = self._prev_timestamp + \
                self._config.output["timestamps"]["dup_add_ns"]
        self._prev_timestamp = timestamp

        vcd.change(gpio_var,
                   timestamp=timestamp,
                   value=val)

    def process_trace_report(self, infile: IO[str], outfile: IO[str]):
        self._first_timestamp = None
        self._prev_timestamp = None
        with self._config.init_vcd(outfile) as vcd:
            for line in infile:
                self._process_trace_report_line(vcd, line)


def run(*, debug: bool = False):
    cli = argparse.ArgumentParser(
        description=textwrap.dedent(
            """\
            GPIO Tracing Logic Analyzer
            A dumb logic analyzer for generating VCD files from Linux tracing event reports.
            """)
    )
    cli.add_argument("-i", "--input", metavar="REPORT",
                     type=argparse.FileType("r"), default=sys.stdin)
    cli.add_argument("-o", "--output", metavar="VCD",
                     type=argparse.FileType("w"), default=sys.stdout)
    cli.add_argument("-C", "--config-file", metavar="FILE",
                     nargs="?", type=argparse.FileType("rb"))
    cli.add_argument("-c", "--config", metavar="KEY=VALUE",
                     action="append", nargs="*", type=Config.parse_str)
    cli.add_argument("command", metavar="CMD", action="extend", nargs="*")
    args = cli.parse_args()

    config = Config(args.config_file)
    config.apply(**(args.config or {}))

    infile = args.input
    if args.command:
        infile = CommandTracer(args.command, debug=debug).trace()

    la = LogicAnalyzer(config, debug=debug)
    la.process_trace_report(infile, args.output)


if __name__ == "__main__":
    sys.exit(run(debug=True))

# TODO: Convert files from `{perf|trace-cmd} report` to value change dump files
# <https://en.wikipedia.org/wiki/Value_change_dump>
# <https://pyvcd.readthedocs.io/en/latest/>
