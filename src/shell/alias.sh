ls_base_options="--group-directories-first"
if command -v dircolors &>/dev/null; then
	ls_base_options="--color=auto $ls_base_options"

	# Grep
	alias grep='grep --color=auto'
	alias egrep='egrep --color=auto'
	alias fgrep='fgrep --color=auto'
	alias rgrep='rgrep --color=auto'
fi

ls_long_options="-lahv $ls_base_options"
alias ls="ls $ls_base_options"
alias l="ls $ls_long_options"
alias ll="ls $ls_long_options"

alias dirs='dirs -lpv'

alias t='tree -a --dirsfirst --noreport'
alias tt='tree -a --dirsfirst --info'

# "less" but start at the end
# `-n` is needed to prevent line number calculation, which takes some time for
# big files
alias eless='less -RSin +G'

# Safety
if [ "$config_alias_rm_safe" = y ]; then
	alias rm='rm -I'
	alias rmf='rm'
fi

# Git
alias g='git'
alias gs='git status --short'
alias gss='git status --long'
if command -v git-saveme &>/dev/null; then alias gS='git-saveme'; fi

# Copy/paste
if [ "$XDG_SESSION_TYPE" = "wayland" ] && command -v wl-copy wl-paste &>/dev/null; then
	alias copy='wl-copy'
	alias paste='wl-paste -n'
elif command -v xclip &>/dev/null; then
	alias copy='xclip -selection clipboard -filter -in'
	alias paste='xclip -selection clipboard -out'

	# Inline copy/paste: when text is copied with a terminating new-line,
	# it is pasted below cursor in vim.  When it is not, it will be pasted
	# right after the cursor.
	alias icopy='xclip -selection clipboard -filter -in -rmlastnl'
	alias ipaste='xclip -selection clipboard -out -rmlastnl'
fi

# Other
alias rp='realpath'

if command -v xdg-open &>/dev/null; then alias open='xdg-open'; fi
if command -v ts &>/dev/null;       then alias tsi='ts -i "%.S"'; fi

if command -v gdb-multiarch &>/dev/null; then
	alias gdb='gdb-multiarch --quiet'
else
	alias gdb='gdb --quiet'
fi

if command -v notify-send &>/dev/null; then
	# Add an "alert" alias for long running commands.  Use like so:
	#   sleep 10; alert
	alias alert='notify-send --urgency=low -i "$([ $? = 0 ] && echo terminal || echo erro)" "$(history|tail -n1|sed -e '\''s/^\s*[0-9]\+\s*//;s/[;&|]\s*alert$//'\'')"'
fi

# Print a number as a hex/octal value
alias hex='printf "%#x\n"'
alias octal='printf "0o%o\n"'

# Check if patches apply to a tree
alias pn='patch -p1 -g1 --dry-run'

if command -v nvim &>/dev/null; then alias n='nvim'; fi

# Work-around for Bear issue[^1]
# [^1]: <https://github.com/rizsotto/Bear/issues/498>
alias bear='no_proxy="localhost,127.0.0.1" GRPC_DNS_RESOLVER=native bear'

alias rsync='rsync --progress'

# nocolor/noansi derived from a stackoverflow answer[^1] by @meustrus
#
# [^1]: <https://stackoverflow.com/a/51141872/3663147>

# Remove all ANSI color codes (other escape codes will remain)
__nocolor_pattern='s/\x1B\[[0-9;]\{1,\}[A-Za-z]//g'
alias nocolor='sed $__nocolor_pattern'
# Remove all ANSI escape codes
__noansi_pattern='s/\x1B[@A-Z\\\]^_]\|\x1B\[[0-9:;<=>?]*[-!"#$%&'"'"'()*+,.\/]*[][\\@A-Z^_`a-z{|}~]//g'
alias noansi='sed $__noansi_pattern'

if command -v xdg-open &>/dev/null && command -v python &>/dev/null; then
	function rfc() {
		local query
		query="$(
			python - "$@" <<-EOM
				import urllib.parse, sys;
				print(urllib.parse.quote(' '.join(sys.argv[1:])))
			EOM
		)"
		xdg-open "https://datatracker.ietf.org/doc/search?rfcs=on&name=$query" &>/dev/null
	}
fi

if command -v curl &>/dev/null; then
	function cheat() {
		local args="$*"
		curl -s -X GET --request-target "/${args:-:help}" 'http://cheat.sh'
	}
	alias cht='cheat'
fi
