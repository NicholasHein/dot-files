# Usage: watch_file_removed_and_then <file> [<cmd>...]
#
# Utility that runs a command when all parents exist and the child changes.
#
# Example (setup a file so that it cannot be removed):
#      watch_file_removed_and_then a/b/c.trigger touch a/b/c.trigger
#
watch_file_removed_and_then() {
	local file=${1:?}
	local cmd=${@:2}

	local dir=$(dirname "$file")
	if [ -d "$dir"/ ]; then
		if ! [ -f "$file" ]; then
			eval ${cmd:-:}
		fi
		watch_file "$file"
	else
		watch_file "$dir"/
	fi
}
