# ~/.profile

append_path () {
	case ":$PATH:" in
	*:"$1":*) ;;
	*) PATH="${PATH:+$PATH:}$1";;
	esac
}

if test -d ~/profile.d/; then
	for profile in ~/profile.d/*.sh; do
		test -r "$profile" || continue
		. "$profile"
	done
	unset profile
fi
