# Common exported and non-exported shell environment

HISTSIZE=50000
# HISTFILESIZE: bash
HISTFILESIZE=10000
# SAVEHIST: zsh
SAVEHIST=$HISTFILESIZE

# don't put duplicate lines or lines starting with space in the history.
# See bash(1) for more options
HISTCONTROL=ignoreboth

export $(locale)

export ESCDELAY=0

# XDG user directories
: ${XDG_CONFIG_HOME:=~/.config}
: ${XDG_CACHE_HOME:=~/.cache}
: ${XDG_DATA_HOME:=~/.local/share}
: ${XDG_STATE_HOME:=~/.local/state}
export XDG_CONFIG_HOME XDG_CACHE_HOME XDG_DATA_HOME XDG_STATE_HOME

# XDG system directories
: ${XDG_DATA_DIRS:=/usr/local/share:/usr/share}
: ${XDG_CONFIG_DIRS:=/etc/xdg}
export XDG_DATA_DIRS XDG_CONFIG_DIRS

function PATH_add() {
	local path
	for path in $*; do
		path=$(realpath -q "$path")
		[ -d "$path" ] || continue

		case :"$PATH": in
			*:"$path":*) ;;
			*) export PATH="$path":"$PATH"
		esac
	done
}

if command -v cargo &>/dev/null; then
	if [ -r ~/.cargo/env ]; then
		source ~/.cargo/env
	elif [ -d ~/.cargo/bin ]; then
		PATH_add ~/.cargo/bin
	fi
fi
PATH_add ~/.local/bin

# Color
case "$TERM" in
	*-color | *-256color) color=yes;;
esac

if [ "$config_color_force" = y ] && \
	command -v tput &>/dev/null && \
	tput setaf 1 &>/dev/null; then
	color=yes
fi

# Program configuration
export CFLAGS='-g -Og'
export CXXFLAGS='-g -Og'

# Buildroot downloads
export BR2_DL_DIR="$XDG_CACHE_HOME"/buildroot/dl/

if command -v ccache &>/dev/null; then
	export USE_CCACHE=1
	export CCACHE_DIR="$XDG_CACHE_HOME"/ccache
	export CCACHE_TEMPDIR="$XDG_CACHE_HOME"/ccache

	# Buildroot ccache
	export BR2_USE_CCACHE=$USE_CCACHE
	export BR2_CCACHE_DIR=$CCACHE_DIR
fi

if command -v dircolors &>/dev/null; then
	eval "$(dircolors -b $(realpath -qe ~/.dircolors || true))"
fi

export LESS="-RSi"
if command -v lesspipe &>/dev/null; then
	eval "$(lesspipe)"
fi

if command -v nproc &>/dev/null; then
	export MAKEFLAGS="${MAKEFLAGS} -j$(($(nproc) + 1))"
fi

if command -v gpg tty &>/dev/null; then
	export GPG_TTY=$(tty)
fi

if command -v journalctl &>/dev/null; then
	# Default is "-FRSXMK"
	export SYSTEMD_LESS="${LESS} -FXMK"
fi

if command -v xauth &>/dev/null; then
	: ${XAUTHORITY:=~/.Xauthority}
	export XAUTHORITY
fi

if command -v git &>/dev/null; then
	export GIT_DISCOVERY_ACROSS_FILESYSTEM=1
fi

if command -v nvim &>/dev/null; then
	EDITOR=nvim
	VISUAL=nvim
	export EDITOR VISUAL
fi

if command -v rg &>/dev/null; then
	export RIPGREP_CONFIG_PATH="$XDG_CONFIG_HOME"/ripgrep/ripgreprc
fi

if command -v minicom &>/dev/null; then
	export MINICOM='-l -con -tmc'
fi

if command -v direnv &>/dev/null; then
	eval "$(direnv hook "$(basename $0)" 2>/dev/null || true)"
fi

# User overrides:
if [ "$config_common_envdir" = y ] && [ -d "$XDG_CONFIG_HOME"/env.d/ ]; then
	for file in "$XDG_CONFIG_HOME"/env.d/*.env; do
		#set -a
		source "$file" || true
		#set +a
	done
	unset cfg
fi
