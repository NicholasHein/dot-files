#!/usr/bin/env zsh

TMUX_CONF_FILE_PATH='~/.tmux.conf'

function print_usage() {
	echo -n "\
usage: ssh-tmux <SSH_HOST>[:<TMUX_SESSION>][!]
Connect to a tmux session over SSH.

    <SSH_HOST>      SSH host name or IP address
    <TMUX_SESSION>  Tmux session name
    !               Detach all other clients first
" >&2
}

main() {
	if [ $# -eq 0 ]
	then
		print_usage
		return 1
	fi

	CONN_PARAMS="$1"
	shift
	CONN_PARAMS_PARTS=(${(s/:/)CONN_PARAMS})

	SSH_HOST=${CONN_PARAMS_PARTS[1]}
	TMUX_SESS_STR=${CONN_PARAMS_PARTS[2]}
	TMUX_SESS_ATTR=${TMUX_SESS_STR[-1]}
	if [[ $TMUX_SESS_ATTR == '!' ]]
	then
		TMUX_ATTACH_PARAMS="-d"
		TMUX_NEWSESS_PARAMS="-D"
		TMUX_SESS="${TMUX_SESS_STR:0:-1}"
	else
		TMUX_ATTACH_PARAMS=''
		TMUX_NEWSESS_PARAMS=''
		TMUX_SESS="${TMUX_SESS_STR}"
	fi

	declare -a TMUX_CMD=()
	local ssh_tmux=0
	if [ -n "$TMUX" ]; then
		ssh_tmux=1
	fi
	TMUX_CMD+=('tmux')

	if [ -n "$TMUX_SESS" ]
	then
		TMUX_CMD+=('new-session'
						'-A' "$TMUX_NEWSESS_PARAMS"
						'-e' "SSH_TMUX=${ssh_tmux}"
						'-s' "$TMUX_SESS")
	else
		TMUX_CMD+=('attach-session'
						'-e' "SSH_TMUX=${ssh_tmux}"
						"$TMUX_ATTACH_PARAMS")
		TMUX_CMD+=('||'
						'tmux'
						'new-session'
						'-e' "SSH_TMUX=${ssh_tmux}")
	fi

	# echo "ssh -t $@ $SSH_HOST \"$TMUX_CMD_STR\""
	ssh -t $@ $SSH_HOST "${TMUX_CMD[@]}"
}

main $@
exit $?
