# XDG user directories
: ${XDG_CONFIG_HOME:=~/.config}
: ${XDG_CACHE_HOME:=~/.cache}
: ${XDG_DATA_HOME:=~/.local/share}
: ${XDG_STATE_HOME:=~/.local/state}
export XDG_CONFIG_HOME XDG_CACHE_HOME XDG_DATA_HOME XDG_STATE_HOME

# XDG system directories
: ${XDG_DATA_DIRS:=/usr/local/share:/usr/share}
: ${XDG_CONFIG_DIRS:=/etc/xdg}
export XDG_DATA_DIRS XDG_CONFIG_DIRS

for profiledir in ~/.local/lib/profile.d "$XDG_DATA_HOME"/profile.d "$XDG_CONFIG_HOME"/profile.d; do
	test -d "$profiledir" || continue
	for profile in "$profiledir"/*.sh; do
		test -r "$profile" || continue
		. "$profile"
	done
	unset profile
done
unset profiledir
