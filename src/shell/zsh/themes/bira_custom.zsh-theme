# This is a customized fork of `bira.zsh-theme`
local return_code="%(?..%{$fg[red]%}%? ↵%{$reset_color%})"
local user_host="%B%(!.%{$fg[red]%}.%{$fg[green]%})%n@%m%{$reset_color%} "
local user_symbol='%(!.#.$)'
local current_dir="%B%{$fg[blue]%}%~ %{$reset_color%}"

local vcs_branch='$(git_prompt_info)'
local venv_prompt='$(virtualenv_prompt_info)'
local mode_prompt='$(vi_mode_prompt_info)'

ZSH_THEME_RVM_PROMPT_OPTIONS="i v g"

PROMPT="╭─${user_host}${current_dir}${mode_prompt}${vcs_branch}${venv_prompt}
╰─%B${user_symbol}%b "
RPROMPT="%B${return_code}%b"

ZSH_THEME_GIT_PROMPT_PREFIX="%{$fg[yellow]%}‹"
ZSH_THEME_GIT_PROMPT_SUFFIX="› %{$reset_color%}"
ZSH_THEME_GIT_PROMPT_DIRTY="%{$fg[red]%}●%{$fg[yellow]%}"
ZSH_THEME_GIT_PROMPT_CLEAN="%{$fg[yellow]%}"

ZSH_THEME_VIRTUAL_ENV_PROMPT_PREFIX="%{$fg[green]%}‹["
ZSH_THEME_VIRTUAL_ENV_PROMPT_SUFFIX="]› %{$reset_color%}"
ZSH_THEME_VIRTUALENV_PREFIX="$ZSH_THEME_VIRTUAL_ENV_PROMPT_PREFIX"
ZSH_THEME_VIRTUALENV_SUFFIX="$ZSH_THEME_VIRTUAL_ENV_PROMPT_SUFFIX"

ZSH_THEME_PROMPT_MODE_PROMPT_PREFIX="%{$fg_bold[cyan]%}--"
ZSH_THEME_PROMPT_MODE_PROMPT_SUFFIX="-- %{$reset_color%}"

# 0, 1 - Blinking block
# 2 - Solid block
# 3 - Blinking underline
# 4 - Solid underline
# 5 - Blinking line
# 6 - Solid line
ZSH_THEME_CURSOR_MAIN=6;    # VI insert (default)
ZSH_THEME_CURSOR_VIINS=6;   # VI insert
ZSH_THEME_CURSOR_ISEARCH=6; # Inc. search
ZSH_THEME_CURSOR_COMMAND=6; # Read command name
ZSH_THEME_CURSOR_VICMD=2;   # VI command (block)
ZSH_THEME_CURSOR_VISUAL=6;  # VI visual (block)
ZSH_THEME_CURSOR_VIOPP=0;   # VI operation pending (block)

# vim:ft=zsh:
