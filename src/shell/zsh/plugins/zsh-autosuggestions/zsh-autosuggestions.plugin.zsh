search_paths=(
	/usr/share/zsh/plugins/zsh-autosuggestions/zsh-autosuggestions.plugin.zsh
	/usr/share/zsh-autosuggestions/zsh-autosuggestions.zsh
)

for plugin ($search_paths); do
	test -r "$plugin" || continue
	source "$plugin"
	return
done

echo "[zsh-autosuggestions] wrapper could not find plugin"
return 1
