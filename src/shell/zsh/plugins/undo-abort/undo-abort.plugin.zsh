function _undo-abort-save-line() {
	if [[ -n "${ZLE_LINE_ABORTED}" ]]; then
		ZSH_ABORTED_LINE="${ZLE_LINE_ABORTED}"
	fi

	if [[ -n "${ZSH_ABORTED_LINE}" ]]; then
		local savebuf="${BUFFER}"
		local savecur="${CURSOR}"
		BUFFER="${ZSH_ABORTED_LINE}"
		CURSOR="${#BUFFER}"

		zle split-undo

		BUFFER="${savebuf}"
		CURSOR="${savecur}"
	fi
	zle _undo-abort-saved_zle-line-init
}

# Alias old widget (if there is one)
zle -A zle-line-init _undo-abort-saved_zle-line-init

# Set this widget
zle -N zle-line-init _undo-abort-save-line
