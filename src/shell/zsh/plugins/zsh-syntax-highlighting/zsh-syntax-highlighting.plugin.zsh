search_paths=(
	/usr/share/zsh/plugins/zsh-syntax-highlighting/zsh-syntax-highlighting.plugin.zsh
	/usr/share/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh
)

for plugin ($search_paths); do
	test -r "$plugin" || continue
	source "$plugin"
	return
done

echo "[zsh-syntax-highlighting] wrapper could not find plugin"
return 1
