function _tmux-renew-env_fixup-gpg-tty() {
	export GPG_TTY="`tty`"
}

declare -a ZSH_TMUX_ENV_FIXUP_FUNCS=(
	_tmux-renew-env_fixup-gpg-tty
)

function _tmux-renew-env_precmd() {
	eval "`tmux show-environment -s 2>/dev/null || true`"
	for func in ${ZSH_TMUX_ENV_FIXUP_FUNCS[*]}; do
		eval "$func"
	done
}

# Only add the hook if we're inside a tmux session
if command -v tmux &>/dev/null && [ -v TMUX ]; then
	add-zsh-hook precmd _tmux-renew-env_precmd
fi
