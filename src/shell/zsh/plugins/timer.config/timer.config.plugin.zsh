# "Patch Plugin" loaded after timer.plugin for configuration

autoload -Uz colors && colors

TIMER_FORMAT="\e[$color[standout];$color[default]m[%d]$reset_color"
TIMER_PRECISION=2
TIMER_THRESHOLD=0.5
