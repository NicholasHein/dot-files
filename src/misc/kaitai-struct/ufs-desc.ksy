meta:
  id: ufsdesc
  title: UFS Descriptor Format
  xref:
    - https://media.digikey.com/pdf/Data%20Sheets/Kingston%20Technology%20PDFs/UFS32G-TXA7-GA20.pdf
    - https://github.com/westerndigitalcorporation/ufs-utils/blob/b5d7a2407f5e7784fa358aa73416e46c695a9bef/ufs_cmds.c
  license: GPL3
  endian: be
  bit-endian: le
seq:
  - id: length
    type: u1
    doc: bLength - Size of the full descriptor
  - id: descriptor_type
    type: u1
    enum: desc_type
    doc: bDescriptorType
  - id: body
    size: length - _io.pos
    type:
      switch-on: descriptor_type
      cases:
        'desc_type::configuration': ufs_config_desc
        'desc_type::unit': ufs_unit_desc
enums:
  desc_type:
    0: device
    1: configuration
    2: unit
    # 3: reserved
    4: interconnect
    5: string
    # 6: reserved
    7: geometry
    8: power
    9: device_health
    10: fbo
    # 11..255: reserved
types:
  ufs_config_desc:
    doc: UFS configuration description.
    seq:
      - id: ufs_config_device_desc
        type:
          switch-on: _parent.length
          cases:
            0x90: ufs_config_device_desc_3_0
            0xe6: ufs_config_device_desc_4_0
      - id: ufs_config_unit_desc
        type:
          switch-on: _parent.length
          cases:
            0x90: ufs_config_unit_desc_3_0
            0xe6: ufs_config_unit_desc_4_0
        repeat: expr
        repeat-expr: 8
    types:
      ufs_config_device_desc_3_0: {}
      ufs_config_device_desc_4_0:
        doc: UFS 4.0 device configuration description.
        seq:
          - id: conf_desc_continue
            type: u1
            doc: |
              bConfDescContinue -
              This value indicates that this is the last Configuration
              Descriptor in a sequence of write descriptor query requests.
              Device shall perform internal configuration based on received
              Configuration Descriptor(s).
          - id: boot_enable
            type: u1
            doc: bBootEnable
          - id: descr_access_en
            type: u1
            doc: bDescrAccessEn
          - id: init_power_mode
            type: u1
            doc: bInitPowerMode
          - id: high_priority_lun
            type: u1
            doc: bHighPriorityLUN
          - id: secure_removal_type
            type: u1
            doc: bSecureRemovalType
          - id: init_active_icc_level
            type: u1
            doc: bInitActiveICCLevel
          - id: periodic_rtc_update
            type: u2
            doc: wPeriodicRTCUpdate - Frequency and method of Real-Time
          - id: hpb_control
            type: u1
            doc: bHPBControl
          - id: rpmb_region_enable
            type: u1
            doc: bRPMBRegionEnable
          - id: rpmb_region_1_size
            type: u1
            doc: bRPMBRegion1Size
          - id: rpmb_region_2_size
            type: u1
            doc: bRPMBRegion2Size
          - id: rpmb_region_3_size
            type: u1
            doc: bRPMBRegion3Size
          - id: write_booster_buffer_preserve_user_space_en
            type: u1
            doc: bWriteBoosterBufferPreserveUserSpaceEn
          - id: write_booster_buffer_type
            type: u1
            doc: bWriteBoosterBufferType
          - id: num_shared_write_booster_buffer_alloc_units
            type: u4
            doc: dNumSharedWriteBoosterBufferAllocUnits
      ufs_config_unit_desc_3_0: {}
      ufs_config_unit_desc_4_0:
        doc: |
          UFS 4.0 unit configuration description.
        seq:
          - id: lu_enable
            type: u1
            enum: lu_enable
            doc: bLUEnable
          - id: boot_lun_id
            type: u1
            enum: boot_lun_id
            doc: bBootLunID
            doc-ref: https://docs.nvidia.com/drive/archive/5.1.0.2L/nvvib_docs/index.html#page/DRIVE_OS_Linux_SDK_Development_Guide%2FInterfaces%2Fprovision_ufs_debugfs.html%23wwpID0E0ZB0HA
          - id: lu_write_protect
            type: u1
            enum: lu_write_protect
            doc: bLUWriteProtect
          - id: memory_type
            type: u1
            enum: memory_type
            doc: bMemoryType
          - id: num_alloc_units
            type: u4
            doc: |
              dNumAllocUnits -

              Number of allocation units of size bAllocationUnitSize (from the
              geometry descriptor) for the LU.

              Geometry Descriptor:
                qTotalRawDeviceCapacity: total size in bytes
                bAllocationUnitSize: number of segments to allocate per "unit"?
                dSegmentSize: number of bytes per segment?

              qTotalRawDeviceCapacity <= dNumAllocUnits * bAllocationUnitSize * dSegmentSize * 512
          - id: data_reliability
            type: u1
            enum: data_reliability
            doc: bDataReliability
          - id: logical_block_size
            type: u1
            doc: bLogicalBlockSize
          - id: provisioning_type
            type: u1
            enum: provisioning_type
            doc: bProvisioningType
          - id: context_capabilities
            type: u2
            doc: wContextCapabilities
          - id: lu_max_active_hpb_regions
            type: u2
            doc: wLUMaxActiveHPBRegions
          - id: hpb_pinned_region_start_idx
            type: u2
            doc: wHPBPinnedRegionStartIdx
          - id: num_hpb_pinned_regions
            type: u2
            doc: wNumHPBPinnedRegions
          - id: lu_num_write_booster_buffer_alloc_units
            type: u4
            doc: dLUNumWriteBoosterBufferAllocUnits
        instances:
          logical_block_size_bytes:
            value: 1 << logical_block_size
        enums:
          lu_enable:
            0: { id: 'no',  doc: 'Logical Unit disabled' }
            1: { id: 'yes', doc: 'Logical Unit enabled' }
          boot_lun_id:
            0: { id: none, doc: 'Not bootable' }
            1: { id: a,    doc: 'Boot LU A' }
            2: { id: b,    doc: 'Boot LU B' }
          lu_write_protect:
            0: { id: 'no',      doc: 'LU not write protected' }
            1: { id: on_wpe,    doc: 'LU write protected when fPowerOnWPEn=1' }
            2: { id: permanent, doc: 'LU permanently write protected when fPermanentWPEn=1' }
          memory_type:
            0: { id: normal,     doc: 'Normal memory' }
            1: { id: sys_code,   doc: 'System code memory type' }
            2: { id: npersist,   doc: 'Non-Persistent memory type' }
            3: { id: enhanced_1, doc: 'Enhanced memory type 1' }
            4: { id: enhanced_2, doc: 'Enhanced memory type 2' }
            5: { id: enhanced_3, doc: 'Enhanced memory type 3' }
            6: { id: enhanced_4, doc: 'Enhanced memory type 4' }
          data_reliability:
            0: { id: 'no',  doc: 'Logical unit is not protected; all data may be lost if there is a power failure during a write operation' }
            1: { id: 'yes', doc: 'Logical unit is protected' }
          provisioning_type:
            0: { id: 'no',         doc: 'Thin provisioning is disabled (default)' }
            2: { id: 'yes_tprz_0', doc: 'Thin provisioning is enabled and TPRZ = 0' }
            3: { id: 'yes_tprz_1', doc: 'Thin provisioning is enabled and TPRZ = 1' }

  ufs_unit_desc:
    doc: UFS 4.0 LUN description.
    seq:
      - id: unit_index
        type: u1
        doc: bUnitIndex
      - id: lu_enable
        type: u1
        doc: bLUEnable
      - id: boot_lun_id
        type: u1
        doc: bBootLunID
      - id: lu_write_protect
        type: u1
        doc: bLUWriteProtect
      - id: lu_queue_depth
        type: u1
        doc: bLUQueueDepth
      - id: psa_sensitive
        type: u1
        doc: bPSASensitive
      - id: memory_type
        type: u1
        doc: bMemoryType
      - id: data_reliability
        type: u1
        doc: bDataReliability
      - id: logical_block_size
        type: u1
        doc: bLogicalBlockSize
      - id: logical_block_count
        type: u8
        doc: qLogicalBlockCount
      - id: erase_block_size
        type: u4
        doc: dEraseBlockSize
      - id: provisioning_type
        type: u1
        doc: bProvisioningType
      - id: phy_mem_resource_count
        type: u8
        doc: qPhyMemResourceCount
      - id: context_capabilities
        type: u2
        doc: wContextCapabilities
      - id: large_unit_cranularity_m1
        type: u1
        doc: bLargeUnitGranularity_M1
      - id: lu_max_active_hpb_regions
        type: u2
        doc: wLUMaxActiveHPBRegions
      - id: hpb_pinned_region_start_idx
        type: u2
        doc: wHPBPinnedRegionStartIdx
      - id: num_hpb_pinned_regions
        type: u2
        doc: wNumHPBPinnedRegions
      - id: lu_num_write_booster_buffer_alloc_units
        type: u4
        doc: dLUNumWriteBoosterBufferAllocUnits
    instances:
      logical_block_size_bytes:
        doc: |
        value: 1 << logical_block_size
      full_size_bytes:
        doc: |
        value: (1 << logical_block_size) * logical_block_count

# vim:ft=yaml:
