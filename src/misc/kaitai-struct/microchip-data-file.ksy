meta:
  id: mpfdat
  title: Microchip PolarFire Data File
  xref:
  license: GPL3
  endian: le
  bit-endian: le
seq:
  - id: header
    type: header_section
  - id: lookup_table
    type: data_lookup_table_entry
    repeat: expr
    repeat-expr: header.const_data.number_of_records
types:
  header_section:
    seq:
      - id: head
        type: header_block
      - id: const_data
        type: constant_data_block
        size: head.header_size - _io.pos
    types:
      header_block:
        doc: |
          Contains information identifying the type of the binary file and data
          size blocks.
        doc-ref: SPI-DirectC User Guide, section 4.1
        seq:
          - id: designer_version_number
            type: str
            size: 24
            encoding: ASCII
          - id: header_size
            type: u1
          - id: image_size
            type: u4
          - id: dat_file_version
            type: u1
          - id: tools_version_number
            type: u2
          - id: map_version_number
            type: u2
          - id: feature_flag
            type: u2
          - id: device_family
            type: u1
      constant_data_block:
        doc: |
          Includes device ID, silicon signature, and other information needed for
          programming.
        doc-ref: SPI-DirectC User Guide, section 4.1
        seq:
          - id: device_id
            type: u4
          - id: device_id_mask
            type: u4
          - id: silicon_signature
            type: u4
          - id: checksum
            type: u2
          - id: number_of_bsr_bits
            type: u2
          - id: number_of_components
            type: u2
          - id: data_size
            type: u2
          - id: erase_data_size
            type: u2
          - id: verify_data_size
            type: u2
          - id: envm_data_size
            type: u2
          - id: envm_verify_data_size
            type: u2
          - id: uek1_exists
            type: u1
          - id: uek2_exists
            type: u1
          - id: sec_erase
            type: u1
          - id: uek3_exists
            type: u1
          - id: padding
            size-eos: true
        instances:
          number_of_records:
            pos: _io.size - 1
            type: u1
          devid:
            value: '(device_id & device_id_mask)'
  data_lookup_table_entry:
    doc: |
      Contains records identifying the starting relative location of all the
      different data blocks used in the SPI-DirectC code and data size of each
      block. The following table lists the format.
    doc-ref: SPI-DirectC User Guide, section 4.1
    seq:
      - id: id
        type: u1
        enum: block_id
      - id: ptr
        type: u4
      - id: len
        type: u4
    enums:
      block_id:
        0: header
        1: user_info
        2: act_urow_design_name
        3: bsr_pattern
        4: bsr_pattern_mask
        5: component_sizes # aka. number of blocks, size of components
        6: upk1
        7: upk2
        8: data_stream # Actual bitstream
        9: erase_data_stream
        10: verify_data_stream
        11: envm_data_stream
        12: envm_verify_data_stream
        13: dpk_id
    instances:
      data:
        io: _root._io
        pos: ptr
        size: len
        type: data_block
        if: len > 0
    types:
      data_block:
        doc: |
          Contains the raw data for all the different variables specified in the
          look-up-table.
        doc-ref: SPI-DirectC User Guide, section 4.1
        seq:
          - id: bin
            type:
              switch-on: _parent.id
              cases:
                'block_id::component_sizes': data_block_component_size_list
                'block_id::data_stream': data_block_data_stream_list
                _: data_block_raw
            size: _io.size - 2
          - id: crc
            type: u2
      data_block_raw:
        seq:
          - id: raw
            size-eos: true
      data_block_component_size_list:
        seq:
          - id: sizes
            type: data_block_component_size
            repeat: expr
            repeat-expr: _root.header.const_data.data_size
        types:
          data_block_component_size:
            seq:
              - id: in_frames
                type: b22
            instances:
              in_bytes:
                value: 'in_frames * 16'
      data_block_data_stream_list:
        seq:
          - id: frame
            size: 16
            repeat: until
            repeat-until: _io.pos + 16 > _io.size

# vim:ft=yaml:
