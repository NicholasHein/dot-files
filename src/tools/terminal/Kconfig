menu "Terminal Tools"

config TOOL_OSC52_COPY
	bool "Copy with OSC 52"
	help
	  Say Y here to install a command-line utility for copying data by
	  sending it in an OSC 52 ANSI escape sequence to the terminal
	  emulator.

	  This is especially useful when used across remote connections, since
	  it bypasses the compositor.  That means you wouldn't need to
	  configure your X11/Wayland clipboard to work across remote logins.

	  Note though, some terminal emulators require additional configuration
	  to allow OSC 52.  For instance, in Alacritty, "terminal.osc52" must
	  be set to either "OnlyCopy" (default) or "CopyPaste" in its config
	  file.

	  If unsure, say no.

config TOOL_OSC52_COPY_TMUX
	bool "Copy in Tmux with OSC 52"
	depends on TOOL_OSC52_COPY
	help
	  Say Y here to install a command-line utility for copying data in Tmux
	  with an ANSI OSC 52 escape sequence.

	  Tmux >= 3.3: "allow-passthrough" must be "on" for this to work.

	  If unsure, say no.

endmenu
