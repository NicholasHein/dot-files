#!/bin/env bash

pretty=0

case "$1" in
	'' | -h | --help)
cat >&2 << EOM
usage: $0 [-h | -p] (<DEC> | 0x<HEX>)...
EOM
		;;
	-p | --pretty) pretty=1 ;;
	*) ;;
esac

(
for arg in $*; do
	num=0
	if [[ "$arg" =~ ^[0-9]+$ ]]; then
		num=$arg
	elif [[ "$arg" =~ ^0x[0-9a-fA-F]+$ ]]; then
		num=$(($arg))
	fi

	bit=0
	binrev=""
	while (( $num )); do
		if (( ($num & (1 << $bit)) == 0 )); then
			binrev+="0"
		else
			binrev+="1"
		fi

		num=$(($num & ~(1 << $bit)))
		bit=$(($bit + 1))

		if (( $pretty )) && (( $num )) && (( $bit % 4 == 0 )); then
			binrev+="_"
		fi
	done

	binary="`echo $binrev | rev`"
	if (( $# == 1 )); then
		echo $binary
	else
		if (( $pretty )); then
			echo "$arg\\==>\\$binary"
		else
			echo "$arg\\$binary"
		fi
	fi
done
) | column -t -s "\\"


# vim:ft=bash:
