#!/bin/env bash

set -o errexit
set -o pipefail

function usage() {
cat >&2 << EOM
usage: ${ME} [-d] telnet://<IP>[:<PORT>]
   or: ${ME} -h

Open a telnet session.  Most useful as the target of an XDG desktop entry.

OPTIONS:
  -d, --desktop     Open as a desktop app
  -h, --help        Show this help message and exit
EOM
}

function err() {
	echo "${ME}: $@" >&2
}

function die() {
	err $@
	exit 1
}

readonly ME="`basename $0`"

pos_args=$(getopt -l "desktop,help" -o "dh" -n "$ME" -- "$@")
eval set -- "$pos_args"

mode="default"
while true; do
	case "$1" in
		"-d" | "--desktop")
			mode="desktop"
			;;
		"-h" | "--help")
			usage
			exit 0;;
		"--")
			shift
			break;;
	esac
	shift
done

uri="$1"

[[ -n "$uri" ]] || {
	err "expected telnet URI"
	usage
	exit 1
}

[[ "$uri" == "telnet://"* ]] \
	|| die "improperly formatted telnet URI"

addr="${uri#telnet://}"

ip="${addr%%:*}"

readonly DEFAULT_PORT=23
if [[ "$addr" == *:* ]]; then
	port="${addr#*:}"
fi
port="${port:-$DEFAULT_PORT}"

[[ -n "$ip" ]] \
	|| die "expected an address"

telnet "$ip" $port || {
	code=$?
	if [[ "$mode" == "desktop" ]]; then
		err "PRESS ANY KEY TO EXIT..."
		read -sn 1
	fi
	exit $code
}

# vim:ft=bash:
