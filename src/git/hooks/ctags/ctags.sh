#!/bin/sh

PATH="/usr/local/bin:$PATH"

GENCTAGS="$(git config 'project.genCtags')"

if [ "$GENCTAGS" = '' ]; then
	exit 0
fi

set -e

git ls-files | \
	ctags -f"tags"
