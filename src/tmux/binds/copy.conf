# Trigger
bind -N "mode: enter copy mode" Space copy-mode

unbind -T copy-mode-vi :
bind -N "Enter command prompt" -T copy-mode-vi : command-prompt

# Begin
bind -N "select: begin selection" -T copy-mode-vi v   send-keys -X begin-selection
bind -N "select: begin rectangle selection" -T copy-mode-vi C-v send-keys -X rectangle-toggle

# Change
bind -N "select: append the selection" -T copy-mode-vi Enter send-keys -X append-selection

# End
bind -N "select: stop selection" -T copy-mode-vi ` send-keys -X stop-selection
bind -N "select: cancel selection" -T copy-mode-vi Escape \
	send-keys -X clear-selection \; \
	switch-client -T copy-mode-vi-escape
bind -N "mode: exit copy mode" -T copy-mode-vi-escape Escape send-keys -X cancel
bind -N "mode: cancel copy mode" -T copy-mode-vi i send-keys -X cancel

# Scroll up/down by 1 line, half screen, whole screen
bind -N "scroll: up 1 line"      -T copy-mode-vi M-Up              send-keys -X scroll-up
bind -N "scroll: down 1 line"    -T copy-mode-vi M-Down            send-keys -X scroll-down
bind -N "scroll: up half page"   -T copy-mode-vi M-PageUp          send-keys -X halfpage-up
bind -N "scroll: down half page" -T copy-mode-vi M-PageDown        send-keys -X halfpage-down
bind -N "scroll: up full page"   -T copy-mode-vi PageUp            send-keys -X page-up
bind -N "scroll: down full page" -T copy-mode-vi PageDown          send-keys -X page-down

# When scrolling with mouse wheel, reduce number of scrolled rows per tick to "2" (default is 5)
set -g @scroll_ticks=2
bind -T copy-mode-vi WheelUpPane       select-pane \; send-keys -X -NF "#{@scroll_ticks}" scroll-up
bind -T copy-mode-vi WheelDownPane     select-pane \; send-keys -X -NF "#{@scroll_ticks}" scroll-down

# Copy/paste
%if "#{==:#{XDG_SESSION_TYPE},wayland}"
set -sg copy-command "wl-copy"
set -sg @paste-command "wl-paste -n"
%elif "#{==:#{XDG_SESSION_TYPE},x11}"
set -sg copy-command "xclip -selection clipboard -i"
set -sg @paste-command "xclip -selection clipboard -o"
%else
display "#[fg=yellow,bold,bright]XDG_SESSION_TYPE#[none]: expected x11 or wayland for clipboard support#[default]"
%endif

unbind p
bind -N "paste: from clipboard" -T prefix p {
	run -b "#{@paste-command} | tmux loadb - && tmux pasteb"
}
bind -N "copy: to clipboard" -T copy-mode-vi y {
	send-keys -X copy-pipe
}
bind -T copy-mode-vi MouseDragEnd1Pane send-keys -X copy-pipe
bind -T copy-mode-vi MouseDown1Pane select-pane \; send-keys -X clear-selection

# Paste buffers
bind -N "buffer: choose" -T prefix C-p choose-buffer -O time

# vim:ft=tmux
