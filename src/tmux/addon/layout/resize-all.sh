#!/bin/bash

set -e

wins=(`tmux list-windows -a -F '#{session_name}:#{window_name}'`)
for win in "${wins[@]}"; do
	if [[ -z "$1" ]]; then
		tmux set-option -uwt "$win" window-size
	else
		tmux set-option -wt "$win" window-size "$1"
	fi
done
