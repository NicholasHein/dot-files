#!/usr/bin/env bash

set -o errexit
set -o pipefail

if [ "$XDG_SESSION_TYPE" = wayland ] && command -v wl-paste &>/dev/null; then
	wl-paste -n
else
	xclip -selection clipboard -o
fi
