#!/usr/bin/env bash

set -o errexit
set -o pipefail

if [ "$XDG_SESSION_TYPE" = wayland ] && command -v wl-copy &>/dev/null; then
	wl-copy
else
	xclip -selection clipboard -i
fi
