vim.g.mapleader = ","

require("bootstrap.lazy").bootstrap():setup {
	{
		"https://gitlab.com/NicholasHein/distro.nvim.git",
		-- NOTE: uncomment the following line to pin the version to tagged releases
		-- version = "*",
		import = "distro.lazy",
		opts = { colorscheme = "nightfox" },
	},
	{
		"EdenEast/nightfox.nvim",
		lazy = false,
		opts = {
			options = {
				transparent = true,
				dim_inactive = false,
			},
		},
	},
	{ import = "distro.lazy.ext.langs.all" },
	-- NOTE: create ~/.config/nvim/lua/plugins.lua before enabling this
	{ import = "plugins", enabled = false },
}
