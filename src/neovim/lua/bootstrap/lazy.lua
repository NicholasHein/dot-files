local uv = vim.loop

---@class LazyNvim
---@field lazypath string
---@field repo_url string
---@field branch string
local LazyNvim = {}
LazyNvim.__index = LazyNvim

---@generic T: LazyNvim
---@param o? T
---@return T
function LazyNvim:new(o)
	return setmetatable(o or {}, self)
end

---Bootstrap Lazy.nvim
---@param force? boolean
---@return boolean
function LazyNvim:bootstrap(force)
	if not force and vim.loop.fs_stat(self.lazypath) then
		return true
	end

	vim.notify("lazy.nvim: bootstrapping...", vim.log.levels.INFO)

	local output = vim.fn.system({
		"git",
		"clone",
		"--filter=blob:none",
		("--branch=%s"):format(self.branch),
		self.repo_url,
		self.lazypath,
	})

	if vim.v.shell_error ~= 0 then
		vim.notify("lazy.nvim: bootstrapping failed!", vim.log.levels.ERROR)
		vim.notify(output, vim.log.levels.DEBUG)
		return false
	end

	vim.notify("lazy.nvim: bootstrapping complete! (run `:checkhealth lazy`)", vim.log.levels.INFO)
	return true
end

---Setup Lazy.nvim
---@param specs any
---@param config? any
function LazyNvim:setup(specs, config)
	local opts = vim.tbl_deep_extend("force", {
		lockfile = vim.fn.stdpath("state") .. "/lazy-lock.json",
		defaults = {
			lazy = false,
			version = false,
		},
		dev = {
			path = uv.os_homedir() .. "/.local/lib/nvim",
			patterns = { "site", "local" },
		},
		install = {
			colorscheme = { "tokyonight", "habamax" },
		},
		checker = {
			enabled = true,
			frequency = 60 * 60 * 24 * 7, -- Weekly
		},
	}, config or {})

	opts.spec = specs

	local lazy = require("lazy")
	lazy.setup(opts)
end

local M = {}

M.LazyNvim = LazyNvim

---Bootstrap Lazy.nvim
---@param o? table Bootstrapping options
---@return LazyNvim?
function M.bootstrap(o)
	local lazypath = vim.fn.stdpath("data") .. "/lazy/lazy.nvim"
	lazypath = vim.env.LAZY or lazypath

	local lazy = LazyNvim:new(vim.tbl_deep_extend("force", {
		lazypath = lazypath,
		repo_url = "https://github.com/folke/lazy.nvim.git",
		branch = "stable",
	}, o or {}))


	local res = lazy:bootstrap()
	if not res then
		return nil
	end
	vim.opt.rtp:prepend(lazypath)

	return lazy
end

return M
