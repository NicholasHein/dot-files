BEGIN {
	FS=""

	# Print the header
	print "# .---------------- minute (0 - 59)"
	print "# |  .------------- hour (0 - 23)"
	print "# |  |  .---------- day of month (1 - 31)"
	print "# |  |  |  .------- month (1 - 12) OR jan,feb,mar,apr ..."
	print "# |  |  |  |  .---- day of week (0 - 6) (Sunday=0 or 7) OR sun,mon,tue,wed,thu,fri,sat"
	print "# |  |  |  |  |"
	print "# m h dom mon dow usercommand"
}

$1 != "#" { print $0 }
