GDB Utilities
=============

User configuration is installed to `~/.gdbinit`.

Utilities
---------

**Getting Started:**

```gdb
source ~/.local/lib/gdb-utils/utils.py
python GdbUtils.init()
```

**Commands:**

* Build
  - `build`: run the build system's configure command
  - `make`: run the build system's compile command
  - `clean`: remove intermediate build files
  - `distclean`: remove *all* build files
* Remote
  - `connect`: extend the target to a remote host
  - `disconn`: exit the connection to a remote host

**Parameters:**

These GDB utilities are manipulated by `set` and `show` commands:

* Build
  - `build`: selects the build system from the following options:
    - `none`: no build system
    - `cmake`: CMake
  - `build dir`: output directory for the build system (default: `$PWD/build`)
  - `build args`: default CLI arguments for the configure command
  - `build env`: environment variables passed to the configure command
* Remote
  - `connect`: select the connection back-end from the following options:
    - `none`: no back-end
    - `ssh`: SSH client
  - `connect host`: set the host for future connections
  - `connect auto`: enable or disable automatically connecting to a host when it is set

**Python Info:**

These GDB utilities are implemented by the package(s) at `~/.local/lib/gdb-utils/python`

Cheatsheet
----------

**General:**

```gdb
file build/my-program
directory
```

**Platform:**

```gdb
set osabi GNU/Linux
set sysroot /path/to/my/sysroot
```

**Remote Debugging:**

```gdb
target extended-remote | ssh -T root@192.168.0.123 gdbserver --multi -
remote put build/my-program /usr/bin/my-remote-program
set remote exec-file /usr/bin/my-remote-program

monitor exit
```

**Logging:**

```gdb
set logging on
set logging file my_file.log
set logging overwrite on
```

**GDB Configuration:**

```gdb
set auto-load safe-path /
source file.gdb
source file.py
add-auto-load-safe-path /path/to/my/libfoo.so.1.2.3-gdb.py
```

Development
-----------

Install development dependencies:

```bash
pip install types-gdb
```
