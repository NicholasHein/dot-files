import gdb
from typing import List, Dict, Mapping


class Builder:
    @property
    def dir(self) -> str:
        return str(gdb.parameter("build dir"))

    @dir.setter
    def dir(self, path: str):
        gdb.set_parameter("build dir", path)

    @property
    def args(self) -> List[str]:
        return list(str(gdb.parameter("build args")).split("\n"))

    @args.setter
    def args(self, args: List[str]):
        gdb.set_parameter("build args", "\n".join(args))

    @property
    def env(self) -> Dict[str, str]:
        envs = dict()
        for line in str(gdb.parameter("build env")).split("\n"):
            pair = dict(enumerate(line.split("=")))
            envs.update({pair[0]: pair.get(1) or ""})
        return envs

    @env.setter
    def env(self, env: Mapping[str, str]):
        envs = "\n".join([f"{k}={v}" for k, v in env.items()])
        gdb.set_parameter("build env", envs)

    def register(self):
        ConfigCommand(self)
        MakeCommand(self)
        CleanCommand(self)
        DistCleanCommand(self)

    def config(self, args: List[str]):
        raise NotImplementedError()

    def make(self, args: List[str]):
        raise NotImplementedError()

    def clean(self):
        raise NotImplementedError()

    def distclean(self):
        raise NotImplementedError()


class DefaultBuilder(Builder):
    def config(self, args: List[str]):
        raise gdb.GdbError("No builder selected!")

    def make(self, args: List[str]):
        raise gdb.GdbError("No builder selected!")

    def clean(self):
        raise gdb.GdbError("No builder selected!")

    def distclean(self):
        raise gdb.GdbError("No builder selected!")


class ConfigCommand(gdb.Command):
    """
    Configure and generate the build scripts.
    Usage: build [CLI_OPTS]...
    """

    def __init__(self, builder: Builder):
        super().__init__("build", gdb.COMMAND_SUPPORT, gdb.COMPLETE_NONE)
        self.__builder = builder

    def invoke(self, arg: str, _from_tty: bool):
        self.dont_repeat()
        argv = gdb.string_to_argv(arg)
        self.__builder.config(argv)


class MakeCommand(gdb.Command):
    """
    Compile the code or call target TARGET.
    Usage: make [TARGET]
    """

    def __init__(self, builder: Builder):
        super().__init__("make", gdb.COMMAND_SUPPORT)
        self.__builder = builder

    def invoke(self, arg: str, _from_tty: bool):
        self.dont_repeat()
        argv = gdb.string_to_argv(arg)

        self.__builder.make(argv)

        progspace = gdb.current_progspace()
        if progspace is None:
            raise RuntimeError()
        gdb.execute(f"file {progspace.filename}")


class CleanCommand(gdb.Command):
    """Remove intermediate build files"""

    def __init__(self, builder: Builder):
        super().__init__("clean", gdb.COMMAND_SUPPORT)
        self.__builder = builder

    def invoke(self, _arg: str, _from_tty: bool):
        self.dont_repeat()
        self.__builder.clean()


class DistCleanCommand(gdb.Command):
    """Remove all build files"""

    def __init__(self, builder: Builder):
        super().__init__("distclean", gdb.COMMAND_SUPPORT)
        self.__builder = builder

    def invoke(self, arg: str, from_tty: bool):
        self.dont_repeat()
        self.__builder.distclean()
