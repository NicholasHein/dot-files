from .builder import Builder
import os.path
import subprocess
from typing import List


class CMakeBuilder(Builder):
    def __init__(
        self,
        *,
        buildtype: str = "Debug",
        c_flags: List[str] = ["-g", "-Og", "-Wall", "-Wextra"],
        cxx_flags: List[str] = ["-g", "-Og", "-Wall", "-Wextra"],
    ):
        super().__init__()
        self.args = [
            f"-DCMAKE_BUILD_TYPE={buildtype}",
            "-DCMAKE_C_FLAGS={}".format(" ".join(c_flags)),
            "-DCMAKE_CXX_FLAGS={}".format(" ".join(cxx_flags)),
        ]

    def config(self, args: List[str]):
        subprocess.run(
            [
                "cmake",
                *self.args,
                *["-S", "."],
                *["-B", self.dir],
            ],
            env=self.env if self.env else None,
            check=True,
        )

    def make(self, args: List[str]):
        subprocess.run(
            [
                "cmake",
                *["--build", self.dir],
                "--parallel",
                *(["--", *args] if args else []),
            ],
            check=True,
        )

    def clean(self):
        self.make(["clean"])

    def distclean(self):
        confirm = input(
            "Removing '{dir}'! Continue? [y/N]".format(
                dir=os.path.abspath(self.dir),
            )
        )
        if confirm in ["y", "Y"]:
            subprocess.run(
                ["rm", "-rf", self.dir],
                check=True,
            )
        else:
            print("aborted!")
