import gdb
from .builder import DefaultBuilder
from .cmake import CMakeBuilder

BUILD_SYSTEMS = {
    "none": DefaultBuilder,
    "cmake": CMakeBuilder,
}


class BuildParameter(gdb.Parameter):
    def __init__(self):
        help_template = "{} the current build system"
        self.set_doc = help_template.format("Set")
        self.show_doc = help_template.format("Show")
        self.value = "none"
        super().__init__(
            "build", gdb.COMMAND_SUPPORT, gdb.PARAM_ENUM, BUILD_SYSTEMS.keys()
        )

    def get_set_string(self):
        BUILD_SYSTEMS[self.value]().register()


class BuildDirParameter(gdb.Parameter):
    def __init__(self):
        help_template = "{} the output directory for the build system"
        self.set_doc = help_template.format("Set")
        self.show_doc = help_template.format("Show")
        self.value = "build"
        super().__init__("build dir", gdb.COMMAND_SUPPORT, gdb.PARAM_FILENAME)


class BuildArgsParameter(gdb.Parameter):
    def __init__(self):
        help_template = "\n".join(
            [
                "{} default CLI arguments for the configure command",
                "Use a newline to separate arguments",
            ]
        )
        self.set_doc = help_template.format("Set")
        self.show_doc = help_template.format("Show")
        self.value = ""
        super().__init__("build args", gdb.COMMAND_SUPPORT, gdb.PARAM_STRING)


class BuildEnvParameter(gdb.Parameter):
    def __init__(self):
        help_template = "\n".join(
            [
                "{} environment variables passed to the configure command",
                "Use `=` to separate each key and value",
                "Use `\\n` to separate each key-value pair",
            ]
        )
        self.set_doc = help_template.format("Set")
        self.show_doc = help_template.format("Show")
        self.value = ""
        super().__init__("build env", gdb.COMMAND_SUPPORT, gdb.PARAM_STRING)


class Parameters:
    @staticmethod
    def register():
        BuildParameter()
        BuildDirParameter()
        BuildEnvParameter()
