import gdb


class Connection:
    @staticmethod
    def extend_piped(cmd: str):
        gdb.execute(f"target extended-remote | {cmd}")

    @property
    def host(self) -> str:
        return str(gdb.parameter("connect host"))

    @host.setter
    def host(self, host: str):
        gdb.set_parameter("connect host", host)

    def register(self):
        ConnectCommand(self)
        DisconnCommand(self)

    def connect(self, host: str):
        raise NotImplementedError()

    def disconn(self):
        gdb.execute("monitor exit")


class DefaultConnection(Connection):
    def connect(self, *_):
        raise gdb.GdbError("No connection selected!")

    def disconn(self):
        raise gdb.GdbError("No connection selected!")


class ConnectCommand(gdb.Command):
    """
    Connect to a remote machine.
    Usage: connect [HOST]
    """

    def __init__(self, connection: Connection):
        super().__init__("connect", gdb.COMMAND_SUPPORT, gdb.COMPLETE_NONE)
        self.__connection = connection

    def invoke(self, arg: str, _from_tty: bool):
        self.dont_repeat()
        self.__connection.connect(arg if arg else self.__connection.host)


class DisconnCommand(gdb.Command):
    """
    Disconnect from a remote machine.
    Usage: disconn
    """

    def __init__(self, connection: Connection):
        super().__init__("disconn", gdb.COMMAND_SUPPORT, gdb.COMPLETE_NONE)
        self.__connection = connection

    def invoke(self, arg: str, _from_tty: bool):
        self.dont_repeat()
        self.__connection.disconn()
