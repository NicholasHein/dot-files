import gdb
from .connection import DefaultConnection
from .ssh import SSHConnection

CONNECTION_BACKENDS = {
    "none": DefaultConnection,
    "ssh": SSHConnection,
}


class ConnectParameter(gdb.Parameter):
    def __init__(self):
        help_template = "{} the back-end for remote connections"
        self.set_doc = help_template.format("Set")
        self.show_doc = help_template.format("Show")
        self.value = "none"
        super().__init__(
            "connect",
            gdb.COMMAND_SUPPORT,
            gdb.PARAM_ENUM,
            CONNECTION_BACKENDS.keys(),
        )

        def get_set_string(self):
            CONNECTION_BACKENDS[self.value]().register()


class ConnectHostParameter(gdb.Parameter):
    def __init__(self):
        help_template = "{} the host for remote connections"
        self.set_doc = help_template.format("Set")
        self.show_doc = help_template.format("Show")
        self.value = ""
        super().__init__("connect host", gdb.COMMAND_SUPPORT, gdb.PARAM_STRING)

    def get_set_string(self):
        if gdb.parameter("connect auto"):
            gdb.execute("connect")


class ConnectAutoParameter(gdb.Parameter):
    def __init__(self):
        help_template = (
            "{} a flag to enable/disable auto connections when a host is set"
        )
        self.set_doc = help_template.format("Set")
        self.show_doc = help_template.format("Show")
        self.value = False
        super().__init__(
            "connect auto",
            gdb.COMMAND_SUPPORT,
            gdb.PARAM_BOOLEAN,
        )


class Parameters:
    @staticmethod
    def register():
        ConnectParameter()
        ConnectHostParameter()
        ConnectAutoParameter()
