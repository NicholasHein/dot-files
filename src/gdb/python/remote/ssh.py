from .connection import Connection


class SSHConnection(Connection):
    def __init__(self):
        super().__init__()

    def connect(self, host: str):
        SSHConnection.extend_piped(f"ssh -T {host} gdbserver --multi -")
