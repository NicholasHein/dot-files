import gdb


class Printer(gdb.printing.PrettyPrinter):
    def __init__(self, type_name, printer):
        super().__init__(type_name)
        self.printer = printer

    def __call__(self, val):
        return self.printer(val) if val.type.name == self.name else None


def type_printer(type_name):
    def _register_printer(printer):
        gdb.printing.register_pretty_printer(
            None,
            Printer(type_name, printer),
        )

    return _register_printer
